variable "service_ports" {
  type        = list(string)
  description = "Ports to allow on Firewall"
  default = [
    "443",
    "80",
  ]
}

variable "web_ip_fqdn" {
  type        = string
  description = "Domain FQDN"
}

variable "backend_service_link" {
  type        = string
  description = "Backend service to map URL to"
}

variable "create_http_forward" {
  type        = bool
  description = "Creates the http fowarding rule"
  default     = false
}

variable "gitlab_zone" {
  type        = string
  description = "Zone name for creating dns records (with trailing period)"
}

variable "name" {
  type        = string
  description = "The pet name"
}

variable "environment" {
  type        = string
  description = "The environment name"
}

variable "tags" {
  type        = list(string)
  description = "Target tags for access by Google LBs. Defaults to [var.name]"
  default     = []
}

variable "firewall_allow_subnets" {
  type        = list(string)
  description = "Subnets allowed to access the IAP, defaults to []"
  default     = []
}

variable "dns_ttl" {
  type        = number
  description = "DNS TTL"
  default     = 300
}

variable "proxied" {
  type        = bool
  description = "Proxy with Cloudflare"
  default     = false
}
