resource "google_compute_global_address" "default" {
  name = format("%v-%v", var.environment, var.name)
}

resource "google_compute_url_map" "default" {
  name = format("%v-%v", var.environment, var.name)

  default_service = var.backend_service_link

  host_rule {
    hosts        = [var.web_ip_fqdn]
    path_matcher = var.name
  }

  path_matcher {
    name            = var.name
    default_service = var.backend_service_link

    path_rule {
      paths   = ["/*"]
      service = var.backend_service_link
    }
  }
}

resource "google_compute_ssl_policy" "default" {
  name = format("%v-%v", var.environment, var.name)

  min_tls_version = "TLS_1_2"
  profile         = "RESTRICTED"
}

resource "google_compute_target_http_proxy" "default" {
  count = var.create_http_forward ? 1 : 0

  name        = format("%v-%v", var.environment, var.name)
  description = "https proxy for performance"

  url_map = google_compute_url_map.default.self_link
}

resource "google_compute_target_https_proxy" "default" {
  name        = format("%v-%v", var.environment, var.name)
  description = "https proxy for performance"

  ssl_certificates = [google_compute_managed_ssl_certificate.default.id]
  url_map          = google_compute_url_map.default.self_link
  ssl_policy       = google_compute_ssl_policy.default.self_link
}

resource "google_compute_global_forwarding_rule" "http" {
  count = var.create_http_forward ? 1 : 0

  name = "${format("%v-%v", var.environment, var.name)}-http"

  target     = google_compute_target_http_proxy.default[0].self_link
  port_range = "80"
  ip_address = google_compute_global_address.default.address
}

resource "google_compute_global_forwarding_rule" "https" {
  name = "${format("%v-%v", var.environment, var.name)}-https"

  target     = google_compute_target_https_proxy.default.self_link
  port_range = "443"
  ip_address = google_compute_global_address.default.address
}
