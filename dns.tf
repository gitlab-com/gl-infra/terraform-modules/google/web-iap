module "dns_record" {
  source  = "ops.gitlab.net/gitlab-com/dns-record/dns"
  version = "3.16.1"

  zone = var.gitlab_zone

  a = {
    (var.web_ip_fqdn) = {
      records = [google_compute_global_address.default.address]
      ttl     = var.dns_ttl
      proxied = var.proxied
    }
  }
}
