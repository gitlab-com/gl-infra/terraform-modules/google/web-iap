resource "google_compute_managed_ssl_certificate" "default" {
  name = format("%v-%v", var.environment, var.name)

  managed {
    domains = [var.web_ip_fqdn]
  }
}
