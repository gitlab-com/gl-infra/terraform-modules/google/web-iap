# GitLab.com Web IAP Terraform Module

## What is this?

This module provisions an IAP-enabled GCE load balancer.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_dns_record"></a> [dns\_record](#module\_dns\_record) | ops.gitlab.net/gitlab-com/dns-record/dns | 3.16.1 |

## Resources

| Name | Type |
|------|------|
| [google_compute_firewall.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_global_address.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_global_address) | resource |
| [google_compute_global_forwarding_rule.http](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_global_forwarding_rule) | resource |
| [google_compute_global_forwarding_rule.https](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_global_forwarding_rule) | resource |
| [google_compute_managed_ssl_certificate.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_managed_ssl_certificate) | resource |
| [google_compute_ssl_policy.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_ssl_policy) | resource |
| [google_compute_target_http_proxy.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_target_http_proxy) | resource |
| [google_compute_target_https_proxy.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_target_https_proxy) | resource |
| [google_compute_url_map.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_url_map) | resource |
| [google_compute_lb_ip_ranges.ranges](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_lb_ip_ranges) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_backend_service_link"></a> [backend\_service\_link](#input\_backend\_service\_link) | Backend service to map URL to | `string` | n/a | yes |
| <a name="input_create_http_forward"></a> [create\_http\_forward](#input\_create\_http\_forward) | Creates the http fowarding rule | `bool` | `false` | no |
| <a name="input_dns_ttl"></a> [dns\_ttl](#input\_dns\_ttl) | DNS TTL | `number` | `300` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name | `string` | n/a | yes |
| <a name="input_firewall_allow_subnets"></a> [firewall\_allow\_subnets](#input\_firewall\_allow\_subnets) | Subnets allowed to access the IAP, defaults to [] | `list(string)` | `[]` | no |
| <a name="input_gitlab_zone"></a> [gitlab\_zone](#input\_gitlab\_zone) | Zone name for creating dns records (with trailing period) | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | The pet name | `string` | n/a | yes |
| <a name="input_proxied"></a> [proxied](#input\_proxied) | Proxy with Cloudflare | `bool` | `false` | no |
| <a name="input_service_ports"></a> [service\_ports](#input\_service\_ports) | Ports to allow on Firewall | `list(string)` | <pre>[<br>  "443",<br>  "80"<br>]</pre> | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Target tags for access by Google LBs. Defaults to [var.name] | `list(string)` | `[]` | no |
| <a name="input_web_ip_fqdn"></a> [web\_ip\_fqdn](#input\_web\_ip\_fqdn) | Domain FQDN | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->FQDN | `string` | n/a | yes |
| <a name="input_zone"></a> [zone](#input\_zone) | n/a | `string` | `""` | no |

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
